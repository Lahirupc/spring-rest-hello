package com.virtusa.nlp;

import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping("/")
	public Greeting sayHi() {
		Greeting g1 = new Greeting("Hello");
		return g1;
	}
}
