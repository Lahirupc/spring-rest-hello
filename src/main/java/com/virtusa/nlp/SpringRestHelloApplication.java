package com.virtusa.nlp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRestHelloApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(SpringRestHelloApplication.class, args);

	}

}
